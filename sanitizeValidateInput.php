<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="sanitizeValidateInput.php" method="post">
        <label>username :</label>
        <input type="text" name="username"><br>
        <label>email :</label>
        <input type="text" name="email"><br>
        <label>password :</label>
        <input type="password" name="password"><br>
        <input type="submit" name="login" value="Log in">
    </form>
</body>

</html>
<?php
if (isset($_POST["login"])) {

    // $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);

    if (empty($email)) {
        echo "No email";
    } else {
        echo "Here is your email : {$email}";
    }
}

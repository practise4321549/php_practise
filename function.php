<?php

// function sing()
// {
//     echo 'I can sing';
// }

// sing();

// // 傳入參數
// function sing($name)
// {
//     echo "{$name} can sing";
// }

// sing('Tom'); // 輸出 Tom can sing


// // 傳入複數參數
// function sing($name, $age, $goodOrBad)
// {
//     echo "{$name} can sing <br>";
//     echo "{$name} sang well at {$age} <br>";
//     echo "{$name} is {$goodOrBad}<br>";
// }

// sing('Tom', 5, 'bad'); // 輸出 Tom can sing
// sing('Emily', 18, 'good'); // 輸出 Emily can sing

// return 結果

function total($num1, $num2)
{
    return $num1 + $num2;
}
echo total(1, 1);

<?php
// 先假設我們有 pizza、apple、juice 三種食物要丟到一個陣列裡面
$foods = array("pizza", "apple", "juice");
// 我們可以這樣輸出
echo $foods[0];
// 輸出 pizza

//哀?如果我想要輸出所有的值那該怎麼辦?該不會要一個一個寫吧? 並沒有!
//在迴圈的單元中會介紹到一個叫 forEach 的方法我們可以簡單地透過這個方法打印出來，

foreach ($foods as $food) {
    echo "{$food}<br>";
}
// 輸出 pizza
//      apple
//      juice

//關聯陣列 (Associative array)

$person = array("name" => "John", "age" => 30, "city" => "New York");
echo $person["name"];  // 输出 "John"
$person["name"] = "Emily";
echo $person["name"];  // 输出 "Emily"
$person["gender"] = "girl";
echo $person["gender"];  // 输出 "girl"
$personKey = array_keys($person);
var_dump($personKey);
$personValues = array_values($person);
var_dump($personValues);
// foreach ($person as $k => $v) {
//     echo "{$k} {$v}<br>";
// }
// 輸出 name John
//      age 30
//      city New York
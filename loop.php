<?php
// for loop
// 重複執行設定的條件直到達成或是中斷，通常用於已知迭代次數的情況。

// for ($i = 0; $i < 5; $i++) {
//     echo $i;
// }
// 輸出 0 1 2 3 4

// while
// 在給定條件為真時重複執行程式碼，適用於不確定迭代次數的情況。

// $i = 0;
// while ($i < 5) {
//     echo $i . " ";
//     $i++;
// }
// 輸出 0 1 2 3 4

//do-while
//與 while 迴圈相似，但不同之處在於 do-while 至少執行一次循環體，然後再檢查條件，適用於不確定迭代次數的情況，且至少希望循環體執行一次。

// $i = 0;
// do {
//     echo $i . " ";
//     $i++;
// } while ($i < 5);

// 輸出 0 1 2 3 4

// forEach
// 用於遍歷數組或物件中的每一個元素的快捷方式，專門用於遍歷數組或物件，不需要設置索引或計數器。

$colors = array("Red", "Green", "Blue");
foreach ($colors as $color) {
    echo $color . " ";
}

// 輸出 Red Green Blue
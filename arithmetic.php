<?php
// 加減乘除取餘數

//加法

$sum = 1 + 1;
echo "加法 : {$sum}";

//減法

$difference = 2 - 1;
echo "<br>減法 : {$difference}"; 

//乘法

$apple = 2 * 2;
echo "<br>乘法 : {$apple}";

//除法

$cup = 4 / 2;
echo "<br>除法 : {$cup}";

//取餘數

$last = 4 % 3;
echo "<br>取餘數 : {$last}";


// 算數賦值運算子 ( Arithmetic Assignment Operators )
$a = 1;
$a +=3; 
echo "<br>算數賦值運算子_加法 : {$a}";

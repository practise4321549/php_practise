<?php
include("header.html");
session_start();
echo "username : " . $_SESSION["username"] . "<br>";
echo "password : " . $_SESSION["password"] . "<br>";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>This is Home page</h2>
    <h3>My practise link</h3>
    <a href="variableAndDataTypes.php">variableAndDataTypes</a>
    <br>
    <a href="arithmetic.php">arithmetic</a>
    <br>
    <a href="incrementAndDecrement.php">incrementAndDecrement</a>
    <br>
    <a href="logicalOperators.php">logicalOperators</a>
    <br>
    <a href="getPost.php">getPost</a>
    <br>
    <a href="function.php">function</a>
    <br>
    <a href="mathFunction.php">mathFunction</a>
    <br>
    <a href="ifElse.php">ifElse</a>
    <br>
    <a href="loop.php">loop</a>
    <br>
    <a href="array.php">array</a>
    <br>
    <a href="isset.php">isset</a>
    <br>
    <a href="sanitizeValidateInput.php">sanitizeValidateInput</a>
    <br>
    <a href="cookie.php">cookie</a>
    <br>
    <a href="databaseDashboard.php">databaseDashboard</a>
    <br>
    <form action="home.php" method="post">
        <input type="submit" name="logout" value="Log out">
    </form>
</body>

</html>
<?php
include("footer.html");

if (isset($_POST["logout"])) {
    session_destroy();
    header("Location: index.php");
}
?>
<?php
/*
public
屬性或方法被聲明為 public 時，它們可以在任何地方被訪問，包括類別外部。這是最寬鬆的可見性，允許任何程式碼訪問屬性或呼叫方法。
private 
屬性或方法被聲明為 private 時，它們只能在宣告它們的類別內部訪問，而在類別外部無法訪問。提供了最高程度的封裝，使得類別的內部實現對外部程式碼不可見。
protected
屬性或方法被聲明為 protected 時，它們只能在宣告它們的類別內部和繼承該類別的子類別中訪問。這提供了某種程度的封裝，使得類別的成員對於外部程式碼是不可見的，但允許在繼承情況下的訪問。
*/

class User
{
    // 設定 class 有哪些屬性
    public $name;
    public $age;
    public $height;

    // 內建一個 function
    public function eat(string $food)
    {
        echo 'I want eat ' . $food;
    }
}
// 創建一個 class 的實例
// $user1 = new User();
// 使用 function
// $user1->eat('pizza');
// 輸出出來
// var_dump($user1);

// 第二個類別
class Car
{
    // 改為 protected 不對外公開，且可以讓子 class 繼承使用
    protected  $brand;
    protected  $year;

    // 建構函式，接受兩個參數，並設定預設值
    public function __construct($brand = 'Unknown', $year = 0)
    {
        $this->brand = $brand;
        $this->year = $year;
    }
    // 取得參數
    public function getInfo()
    {
        return "Brand: {$this->brand}, Year: {$this->year}";
    }

    public function setYear($year)
    {
        $this->year = $year;
    }
}

// 沒有提供參數，將使用預設值
$car1 = new Car();

// 提供部分參數，使用提供的值
$car2 = new Car('Toyota');

// 提供所有參數，使用提供的值
$car3 = new Car('Honda', 2020);

var_dump($car2);
$car2->setYear(2022);
var_dump($car2);

// 子類 - OffRoadCar
class OffRoadCar extends Car
{
    // 新增方法
    public function canFordWater()
    {
        echo "This off-road car {$this->brand} can ford water.";
    }
}

$offRoadCar = new OffRoadCar('Jeep', 2022);
$offRoadCar->canFordWater();

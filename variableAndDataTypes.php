<?php
// 創建一個名為 name 的變數，並將它的值設定為 Emily
$name = 'Emily';

// 使用 echo 將 name 的值輸出到螢幕上
echo "我的名子叫: " . $name . '<br>';

// 透過{}輸出
echo "我的名子叫: {$name} <br>";

//Data Type

//整數型態（Integer）：整數型態用於表示整數，可以是正數、負數或零。
$num = 10;

//浮點數型態（Float）：浮點數型態用於表示帶有小數點的數字。
$floatNum = 3.14;

//字串型態（String）：字串型態用於表示文字或字符。
$text = "Hello, World!";

//布林型態（Boolean）：布林型態只有兩個值：true 或 false，用於表示邏輯的真假。
$isTrue = true;

//陣列型態（Array）：陣列型態用於存儲多個值的集合。
$colors = array("Red", "Green", "Blue");

//物件型態（Object）：物件型態用於創建自定義的數據類型，可以包含屬性和方法。
class Car {
    function run() {
        echo "gogogo! gogogo!";
    }
}

$myCar = new Car();
$myCar->run();

//NULL 型態：NULL 型態表示變數沒有被賦值，或者被顯式地賦值為 null。
$variable = null;

?>
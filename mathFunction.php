<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="mathFunction.php" method="post">
        <label>X :</label>
        <input type="text" name="X"><br>
        <label>Y :</label>
        <input type="text" name="Y"><br>
        <input type="submit" value="Total">
    </form>
</body>

</html>
<?php

echo '第一個參數:' . $_POST["X"] . '<br>';
echo '第二個參數:' . $_POST["Y"] . '<br>';

$x = $_POST["X"];
$y = $_POST["Y"];

$total = null;
// abs() 返回一個數的絕對值。
// $total = abs($x);
// round() 返回最接近的整數。
// $total = round($x);
// floor 返回無條件捨去。
// $total = floor($x);
// ceil() 返回無條件進位
// $total = ceil($x);
// pow() 返回指定數的指定次方。
// $total = pow($x, $y);
// sqrt() 返回一個數的平方根。
// $total = sqrt($x);
// max() 返回一個傳入中的值最大的數。
// $total = max($x, $y);
// min() 返回一個傳入中的值最小的數。
// $total = min($x, $y);
// pi() 返回 3.14... 需要使用到時可以互叫
// rand() 返回一個隨機參數或是給予範圍內的值

// echo '這是abs()的計算結果 :' . $total . '<br>';
// echo '這是round()的計算結果 :' . $total . '<br>';
// echo '這是floor()的計算結果 :' . $total . '<br>';
// echo '這是ceil()的計算結果 :' . $total . '<br>';
// echo '這是pow()的計算結果 :' . $total . '<br>';
// echo '這是sqrt()的計算結果 :' . $total . '<br>';
// echo '這是max()的計算結果 :' . $total . '<br>';
echo '這是min()的計算結果 :' . $total . '<br>';

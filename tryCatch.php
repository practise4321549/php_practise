<?php
function test($a)
{
    if (!is_int($a)) { // 檢查類型
        throw new Exception('This is not int type');
    }
    return $a;
}

try {
    echo test(5);
    echo test('a');
} catch (Exception $e) {
    echo 'Error message : ' . $e->getMessage(); // 接到錯誤

} finally {
    echo 'The last'; // 必定執行
}

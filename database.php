<?php
// 連線主機
$db_server = "localhost";
// 使用者名稱
$db_user = "root";
// 使用者密碼
$db_pass = "";
// DB名稱
$db_name = "practisedb";
$conn = "";

try {
    // 連線變數
    $conn = mysqli_connect($db_server, $db_user, $db_pass, $db_name);
} catch (mysqli_sql_exception) {
    echo "Connected Fail";
}


// 連結狀態對話
if ($conn) {
    echo "You are connected<br>";
}

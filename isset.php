<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="isset.php" method="post">
        <label>username :</label>
        <input type="text" name="username"><br>
        <label>password :</label>
        <input type="password" name="password"><br>
        <input type="submit" name="login" value="Log in">
    </form>
</body>

</html>

<?php
/*
// isset()
// 用於檢查一個變數是否已設置並且不為 null。
// 如果變數已設置並且不為 null，則返回 true；否則返回 false。
// 例子：
$name = "John";
echo "<br>isset" . isset($name);  // 输出 true

$age = null;
echo "<br>isset" . isset($age);   // 输出 false


// empty()
// empty() 用於檢查一個變數是否為空（值為空字符串、空陣列、0、false、null 或未設置）。
// 返回值： 如果變數為空，則返回 true；否則返回 false。
// 例子：
$name = "John";
echo "<br>empty" . empty($name);  // 输出 false

$age = 0;
echo "<br>empty" . empty($age);   // 输出 true

// iis_null()
// is_null() 用於檢查一個變數是否為 null。
// 如果變數為 null，則返回 true；否則返回 false。
// 例子：
$name = null;
echo "<br>is_null" . is_null($name);  // 输出 true

$age = 25;
echo "<br>is_null" . is_null($age);   // 输出 false
*/

// name = key
if (isset($_POST["login"])) {
    echo "You have login button<br>";
    $username = $_POST["username"];
    $password = $_POST["password"];
    if (empty($username)) {
        echo "Your username is missing<br>";
    } elseif (empty($password)) {
        echo "Your password is missing<br>";
    } else {
        echo "Hello {$username}";
    }
}

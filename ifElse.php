<?php
// if else
// $age = 11;
// if ($age >= 18) {
//     echo 'Yes, you can';
// } elseif ($age <= 0) {
//     echo 'NONONO';
// } elseif ($age >= 100) {
//     echo 'wowowo';
// } else {
//     echo 'Too young';
// }

//switch

$age = 20; // 假設年齡為 20 歲

switch (true) {
    case $age >= 18:
        echo "成年人";
        break;
    case $age >= 13:
        echo "青少年";
        break;
    default:
        echo "兒童";
        break;
}
// 最後輸出成年人
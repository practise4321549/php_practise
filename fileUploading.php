<?php
// 確認有沒有送出
if (isset($_POST['submit'])) {
    $allowed_ext = array('png', 'jpg', 'jpeg', 'gif');
    // 確認檔案是否存在 檔案名稱
    if (!empty($_FILES['upload']['name'])) {
        // print_r($_FILES['upload']);
        $file_name = $_FILES['upload']['name'];
        $file_size = $_FILES['upload']['size'];
        $file_tmp_name = $_FILES['upload']['tmp_name'];
        $target_dir = "Uploads/{$file_name}";

        // 確認檔案類型名稱，透過 . 分割檔名後取出最末的副檔名
        $file_ext = explode('.', $file_name);
        $file_ext = strtolower(end($file_ext));

        // 確認是否合法
        if (in_array($file_ext, $allowed_ext)) {
            // 確認檔案大小
            if ($file_size <= 500000) {
                move_uploaded_file($file_tmp_name, $target_dir);
                $message = '<p style="color :green;">上傳成功</p>';
            } else {
                $message = '<p style="color :red;">請選擇正確大小的類型的檔案</p>';
            }
        } else {
            $message = '<p style="color :red;">請選擇規定檔案類型</p>';
        }
    }
} else {
    // 建立警告語
    $message = '<p style="color :red;">請選擇png/jpg類型的檔案</p>';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Uploading</title>
</head>

<body>
    <!--當警告語存在就 echo-->
    <?php echo $message ?? null; ?>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">選擇需要上傳的檔案
        <input type="file" name="upload">
        <input type="submit" name="submit" value="送出">
    </form>
</body>

</html>